var con=1;
var b1="b1",b2="b2",b3="b3",b4="b4",b5="b5",b6="b6",b7="b7",b8="b8",b9="b9";
var jug1=0,jug2=0;
function reiniciar(){
  $("button").removeAttr("value");
  $("button").attr("class","buttons")
  $("button").empty();
  
}
function actions(boton){
 if(boton.className=="buttons"){

  boton.className="clicked";
  var text;
  if(con%2==0){
   text="X";
  }else{
   text="O";
  }
  boton.value=text;

  boton.appendChild(document.createTextNode(text));
  con++;
  asignation(text,boton);
 }else{

 }
}
function asignation(text,boton){
 if(b1==boton.name){
  b1=text;
 }
 else if(b2==boton.name){
  b2=text;
 }
 else if(b3==boton.name){
  b3=text;
 }
 else if(b4==boton.name){
  b4=text;
 }
 else if(b5==boton.name){
  b5=text;
 }
 else if(b6==boton.name){
  b6=text;
 }
 else if(b7==boton.name){
  b7=text;
 }
 else if(b8==boton.name){
  b8=text;
 }
 else if(b9==boton.name){
  b9=text;
 }
 
 validation(text);
}
function validation(text){
 if((b1==b2 && b2==b3)||(b1==b5 && b5==b9)||(b3==b5 && b5==b7)||(b4==b5 && b5==b6)||(b7==b8 && b8==b9)||(b1==b4 && b4==b7)||(b2==b5 && b5==b8)||(b3==b6 && b6==b9)){
  var con=alert(text+" GANA XD");
  if(text=="X"){
    jug1++;
    document.getElementById('jug1').innerHTML=jug1;
  }
  else if(text=="O"){
    jug2++;
    document.getElementById('jug2').innerHTML=jug2;
  }

  
  if(con==true){
    
  // window.location.reload();

  }
 }

}
function descargarArchivo(contenidoEnBlob, nombreArchivo) {
  var reader = new FileReader();
  reader.onload = function (event) {
      var save = document.createElement('a');
      save.href = event.target.result;
      save.target = '_blank';
      save.download = nombreArchivo || 'archivo.dat';
      var clicEvent = new MouseEvent('click', {
          'view': window,
              'bubbles': true,
              'cancelable': true
      });
      save.dispatchEvent(clicEvent);
      (window.URL || window.webkitURL).revokeObjectURL(save.href);
  };
  reader.readAsDataURL(contenidoEnBlob);
};

//Función de ayuda: reúne los datos a exportar en un solo objeto
function obtenerDatos() {
  return {
      nombre:  document.getElementById('jug1').innerHTML,
      nombre2: document.getElementById('jug2').innerHTML,
      fecha: (new Date()).toLocaleDateString()
  };
};

//Función de ayuda: "escapa" las entidades XML necesarias
//para los valores (y atributos) del archivo XML
function escaparXML(cadena) {
  if (typeof cadena !== 'string') {
      return '';
  };
  cadena = cadena.replace('&', '&amp;')
      .replace('<', '&lt;')
      .replace('>', '&gt;')
      .replace('"', '&quot;');
  return cadena;
};

//Genera un objeto Blob con los datos en un archivo TXT
function generarTexto(datos) {
  var texto = [];
  texto.push('Tablero de Score:\n');
  texto.push('Ganadas del Jugador X:-> ');
  texto.push(datos.nombre);
  texto.push('\n');
  texto.push('Ganadas del Jugador O:->');
  texto.push(datos.nombre2);
  texto.push('\n');
  texto.push('Fecha: ');
  texto.push(datos.fecha);
  texto.push('\n');
  //El contructor de Blob requiere un Array en el primer parámetro
  //así qu0e no es necesario usar toString. el segundo parámetro
  //es el tipo MIME del archivo
  return new Blob(texto, {
      type: 'text/plain'
  });
};
document.getElementById('boton-txt').addEventListener('click', function () {
  var datos = obtenerDatos();
  descargarArchivo(generarTexto(datos), 'archivo.txt');
}, false);